// import React from 'react'
import styled from 'styled-components'

const PageLayout = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`

export default PageLayout
