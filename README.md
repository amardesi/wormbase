# Micropubs - Wormbase

This is the repo for the Micropublications.org project

## Roadmap

|Task                       |In progress    |Done       |Issue Number   |
|---                        |---            |---        |---            |
|Set up login/signup        |               |&#x2714;   |#27
|Docker container for db    |               |&#x2714;   |#3
|Private routing            |               |&#x2714;   |#2
|Implement Navbar           |               |&#x2714;   |#14
|Setup Loaders              |               |&#x2714;   |#28
|Linting                    |               |&#x2714;   |#8
|Linting on Commit          |               |&#x2714;   |#24
|Conventional Commits       |               |&#x2714;   |#11
|Unit Tests Scripts         |               |&#x2714;   |#7
|e2e Test Scripts           |               |&#x2714;   |#6
|Decide on Form Lib         |               |&#x2714;   |#18
|Set up Dashboard           |               |&#x2714;   |#16
|Set up Form Submission     |               |&#x2714;   |#17
|Amigo API extension        |               |&#x2714;   |#21
|Wormbase DB API extension  |               |&#x2714;   |#19
|Roles & permissions        |               |&#x2714;   |#19
|Granttome API extension    |               |           |#20
